import styles from './styles.module.css'
import {useNavigate} from "react-router-dom";
import Button from "../../components/button/button";
import ContentWrapper from "../../components/content-wrapper/content-wrapper";

const OrderPage =()=>{
    const navigate = useNavigate()

    return(
        <ContentWrapper className={styles.order}>
           <h1> Ваш заказ будет в ближайшее время</h1>
            <Button
                onClick={()=> navigate('/')}
                containerClassName={styles.button}>
                На Главную
            </Button>
        </ContentWrapper>
    )
}

export default OrderPage