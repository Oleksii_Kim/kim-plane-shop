const express = require('express');
const mongoose = require('mongoose');
const app = express();
require('dotenv').config();

const port = process.env.PORT || 5000;
// const db = 'mongodb+srv://Oleksii:1234@cluster0.th8dqrf.mongodb.net/test?retryWrites=true&w=majority';
app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.use('/static', express.static(__dirname + '/assets'));

app.use('/api/planes', require('./routes/planes'));


mongoose
    .connect('mongodb+srv://Oleksii:1234@cluster0.th8dqrf.mongodb.net/?retryWrites=true&w=majority')
    .then(() => {
        app.listen(port, () => {
            console.log(`App listening on port ${port}`);
        });
    }).catch((error) => console.log(error));

